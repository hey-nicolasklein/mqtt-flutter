# flutter_mqtt_example

A Flutter application for receiving messages through the MQTT communication protocol.
<br/><br/>
![screen2](screenshots/screen2.png "Recieving new MQTT messages.")
![screen1](screenshots/screen1.png "List of received MQTT messages.")






## Getting Started

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
