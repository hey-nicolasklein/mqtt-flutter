import 'package:flutter/material.dart';

class History extends StatelessWidget {

  List<String> _history;

  History(this._history);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _history
          .map((e) => ListTile(
        leading: Icon(Icons.chat),
        title: Text(e),
      ))
          .toList(),
    );
  }
}

