import 'package:flutter/material.dart';
import 'package:flutter_mqtt_location_example/models/mqttClientWrapper.dart';
import 'package:flutter_mqtt_location_example/models/models.dart';
import 'package:flutter_mqtt_location_example/screens/history/history.dart';
import 'package:flutter_mqtt_location_example/screens/main/mqttStream.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter MQTT',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: MyHomePage(title: 'Flutter MQTT'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  List<String> _history = [];

  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: History',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  MQTTClientWrapper _mqttClientWrapper;
  MqttCurrentConnectionState _connectionState;
  String _latestData;

  void setup() {
    _mqttClientWrapper = MQTTClientWrapper(() => gotConnected(),
        (newLocationJson) => gotNewMessage(newLocationJson));
    _mqttClientWrapper.prepareMqttClient();
  }

  void gotNewMessage(String data) {
    setState(() {
      this._latestData = data;
      this._history.add(data);
    });
  }

  void gotConnected() {
    setState(() {
      this._connectionState = MqttCurrentConnectionState.CONNECTED;
    });
  }

  @override
  void initState() {
    super.initState();
    this._latestData = "hi";
    this._connectionState = MqttCurrentConnectionState.IDLE;
    this._history;
    setup();
  }

  Widget getLoadingScreen() {
    Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text("CONNECTING TO MQTT..."),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }

  Widget getBody() {
    switch (_selectedIndex) {
      case 0:
        {
          return MqttStream(this._mqttClientWrapper);
        }
        break;
      case 1:
        {
          return History(this._history);
        }
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.history),
            title: Text('History'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.green,
        onTap: _onItemTapped,
      ),
      body: _mqttClientWrapper.connectionState !=
              MqttCurrentConnectionState.CONNECTED
          ? getLoadingScreen()
          : getBody(),
    );
  }
}
