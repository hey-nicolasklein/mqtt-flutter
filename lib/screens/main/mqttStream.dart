import 'package:flutter/material.dart';
import 'package:flutter_mqtt_location_example/models/mqttClientWrapper.dart';
import 'package:mqtt_client/mqtt_client.dart';

class MqttStream extends StatelessWidget {
  final MQTTClientWrapper _mqttClient;

  MqttStream(this._mqttClient);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<MqttReceivedMessage<MqttMessage>>>(
        stream: _mqttClient.client.updates,
        builder: (context, asyncSnapshot) {
          List<Widget> children;
          if (asyncSnapshot.hasError) {
            children = <Widget>[
              Icon(
                Icons.error_outline,
                color: Colors.red,
                size: 60,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16),
                child: Text('Error: ${asyncSnapshot.error}'),
              )
            ];
          } else {
            switch (asyncSnapshot.connectionState) {
              case ConnectionState.none:
                children = <Widget>[
                  Icon(
                    Icons.info,
                    color: Colors.blue,
                    size: 60,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Select a lot'),
                  )
                ];
                break;
              case ConnectionState.waiting:
                children = <Widget>[
                  SizedBox(
                    child: const CircularProgressIndicator(),
                    width: 60,
                    height: 60,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Awaiting data...'),
                  )
                ];
                break;
              case ConnectionState.active:
                final MqttPublishMessage recMess =
                    asyncSnapshot.data[0].payload;
                final String newDataJson =
                    MqttPublishPayload.bytesToStringAsString(
                        recMess.payload.message);
                children = <Widget>[
                  Icon(
                    Icons.check_circle_outline,
                    color: Colors.green,
                    size: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text('$newDataJson'),
                  )
                ];
                break;
              case ConnectionState.done:
                final MqttPublishMessage recMess =
                    asyncSnapshot.data[0].payload;
                final String newDataJson =
                    MqttPublishPayload.bytesToStringAsString(
                        recMess.payload.message);
                children = <Widget>[
                  Icon(
                    Icons.info,
                    color: Colors.blue,
                    size: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text('$newDataJson (closed)'),
                  )
                ];
                break;
            }
          }
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: children,
            ),
          );
        });
  }
}
